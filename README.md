# Comunidade Ruby on Rails Brasil

Nosso site é gerado usando o [Middleman](https://middlemanapp.com/) e hospedado
pelo [GitLab Pages](http://doc.gitlab.com/ee/pages).

O código fonte encontra-se na branch `source` (para compatibilidade opcional com Github Pages).
A geração do HTML estático é feita automaticamente pelo Gitlab CI (veja `.gitlab-ci.yml`), e publicado
no GitLab pages.

## Contribuir

Este site está aberto a contribuições da comunidade. Sugerimos que leia a [documentação
do Middleman](https://middlemanapp.com/basics/install/) antes de enviar um Merge Request.

Alguns passos simples para começar:

```sh
git clone https://gitlab.com/rubyonrailsbrasil/rubyonrailsbrasil.gitlab.io.git
bundle install

# inicia um servidor local para pré-visualizar modificações:
bundle exec middleman
```
